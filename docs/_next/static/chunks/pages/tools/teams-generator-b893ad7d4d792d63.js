(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [534],
  {
    117: function (t, n, e) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        '/tools/teams-generator',
        function () {
          return e(4026);
        },
      ]);
    },
    5608: function (t, n, e) {
      'use strict';
      e.d(n, {
        Z: function () {
          return i;
        },
      });
      var r = e(5893),
        u = e(9008),
        a = function (t) {
          var n = t.title;
          return (0, r.jsxs)(u.default, {
            children: [
              (0, r.jsx)('title', { children: n }),
              (0, r.jsx)('link', {
                href: 'https://fonts.googleapis.com/css2?family=Lato&display=swap',
                rel: 'stylesheet',
              }),
            ],
          });
        },
        i = function (t) {
          var n = t.children,
            e = t.title;
          return (0, r.jsxs)(r.Fragment, {
            children: [(0, r.jsx)(a, { title: e }), n],
          });
        };
    },
    4026: function (t, n, e) {
      'use strict';
      e.r(n),
        e.d(n, {
          default: function () {
            return v;
          },
        });
      var r = e(5893),
        u = e(7294),
        a = e(5608),
        i = e(947),
        o = e.n(i);
      function s(t) {
        var n = t.autoFocus,
          e = t.setValue,
          a = t.value,
          i = (0, u.useRef)(null);
        (0, u.useEffect)(function () {
          n && i.current && i.current.focus();
        }, []);
        var s = (0, u.useCallback)(
            function (t) {
              e && e(t.target.value);
            },
            [e]
          ),
          l = (0, u.useMemo)(
            function () {
              return a.split('\n').length;
            },
            [a]
          );
        return (0, r.jsx)('textarea', {
          ref: i,
          className: o()['text-area'],
          onChange: s,
          readOnly: !e,
          rows: l,
          value: a,
        });
      }
      function l(t) {
        return (
          (function (t) {
            if (Array.isArray(t)) {
              for (var n = 0, e = new Array(t.length); n < t.length; n++)
                e[n] = t[n];
              return e;
            }
          })(t) ||
          (function (t) {
            if (
              Symbol.iterator in Object(t) ||
              '[object Arguments]' === Object.prototype.toString.call(t)
            )
              return Array.from(t);
          })(t) ||
          (function () {
            throw new TypeError(
              'Invalid attempt to spread non-iterable instance'
            );
          })()
        );
      }
      function c(t) {
        for (var n = l(t), e = n.length - 1; e > 0; e--) {
          var r,
            u = Math.floor(Math.random() * (e + 1));
          (r = [n[u], n[e]]), (n[e] = r[0]), (n[u] = r[1]);
        }
        return n;
      }
      function f(t) {
        var n = t.value,
          e = t.setValue,
          a = t.min,
          i = t.max,
          o = (0, u.useCallback)(function (t) {
            var n = t.target,
              r = n.validity.valid,
              u = n.valueAsNumber;
            r && e(u);
          }, []);
        return (0, r.jsx)('input', {
          type: 'number',
          value: n,
          onInput: o,
          min: a,
          max: i,
        });
      }
      function v() {
        var t = (0, u.useState)(''),
          n = t[0],
          e = t[1],
          i = (0, u.useState)(2),
          o = i[0],
          l = i[1],
          v = (0, u.useState)(''),
          p = v[0],
          m = v[1],
          x = (0, u.useCallback)(
            function () {
              var t = n
                  .split('\n')
                  .filter(function (t) {
                    return t;
                  })
                  .map(function (t) {
                    return t.trim().split(/\s+/);
                  }),
                r = (function (t, n) {
                  for (
                    var e = t.map(c).flat(),
                      r = Array.from({ length: n }, function () {
                        return [];
                      });
                    e.length;

                  )
                    r.forEach(function (t) {
                      var n = e.shift();
                      n && t.push(n);
                    }),
                      e.reverse();
                  return r.filter(function (t) {
                    return t.length;
                  });
                })(t, o);
              e(h(t)), m(h(r));
            },
            [n, m, o]
          );
        return (0, r.jsxs)(a.Z, {
          title: 'Teams Generator',
          children: [
            (0, r.jsx)(s, { value: n, setValue: e, autoFocus: !0 }),
            (0, r.jsx)(f, { value: o, setValue: l, min: 2 }),
            (0, r.jsx)('button', { onClick: x, children: 'Generate' }),
            (0, r.jsx)(s, { value: p }),
          ],
        });
      }
      function h(t) {
        return t
          .map(function (t) {
            return t.join(' ');
          })
          .join('\n');
      }
    },
    947: function (t) {
      t.exports = { 'text-area': 'TextArea_text-area__gbAlr' };
    },
    9008: function (t, n, e) {
      t.exports = e(5443);
    },
  },
  function (t) {
    t.O(0, [774, 888, 179], function () {
      return (n = 117), t((t.s = n));
      var n;
    });
    var n = t.O();
    _N_E = n;
  },
]);
