(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [613],
  {
    4847: function (t, n, e) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        '/books/deep-work',
        function () {
          return e(2976);
        },
      ]);
    },
    5608: function (t, n, e) {
      'use strict';
      e.d(n, {
        Z: function () {
          return u;
        },
      });
      var r = e(5893),
        i = e(9008),
        s = function (t) {
          var n = t.title;
          return (0, r.jsxs)(i.default, {
            children: [
              (0, r.jsx)('title', { children: n }),
              (0, r.jsx)('link', {
                href: 'https://fonts.googleapis.com/css2?family=Lato&display=swap',
                rel: 'stylesheet',
              }),
            ],
          });
        },
        u = function (t) {
          var n = t.children,
            e = t.title;
          return (0, r.jsxs)(r.Fragment, {
            children: [(0, r.jsx)(s, { title: e }), n],
          });
        };
    },
    2976: function (t, n, e) {
      'use strict';
      e.r(n),
        e.d(n, {
          default: function () {
            return s;
          },
        });
      var r = e(5893),
        i = e(5608);
      function s() {
        return (0, r.jsx)(i.Z, { title: 'Deep Work' });
      }
    },
    9008: function (t, n, e) {
      t.exports = e(5443);
    },
  },
  function (t) {
    t.O(0, [774, 888, 179], function () {
      return (n = 4847), t((t.s = n));
      var n;
    });
    var n = t.O();
    _N_E = n;
  },
]);
