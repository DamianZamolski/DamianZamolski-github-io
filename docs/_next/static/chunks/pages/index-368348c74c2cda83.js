(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [405],
  {
    5301: function (a, e, o) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        '/',
        function () {
          return o(9295);
        },
      ]);
    },
    8418: function (a, e, o) {
      'use strict';
      function t(a, e) {
        return (
          (function (a) {
            if (Array.isArray(a)) return a;
          })(a) ||
          (function (a, e) {
            var o = [],
              t = !0,
              i = !1,
              n = void 0;
            try {
              for (
                var l, u = a[Symbol.iterator]();
                !(t = (l = u.next()).done) &&
                (o.push(l.value), !e || o.length !== e);
                t = !0
              );
            } catch (r) {
              (i = !0), (n = r);
            } finally {
              try {
                t || null == u.return || u.return();
              } finally {
                if (i) throw n;
              }
            }
            return o;
          })(a, e) ||
          (function () {
            throw new TypeError(
              'Invalid attempt to destructure non-iterable instance'
            );
          })()
        );
      }
      e.default = void 0;
      var i,
        n = (i = o(7294)) && i.__esModule ? i : { default: i },
        l = o(6273),
        u = o(387),
        r = o(7190);
      var s = {};
      function d(a, e, o, t) {
        if (a && l.isLocalURL(e)) {
          a.prefetch(e, o, t).catch(function (a) {
            0;
          });
          var i =
            t && 'undefined' !== typeof t.locale ? t.locale : a && a.locale;
          s[e + '%' + o + (i ? '%' + i : '')] = !0;
        }
      }
      var b = function (a) {
        var e,
          o = !1 !== a.prefetch,
          i = u.useRouter(),
          b = n.default.useMemo(
            function () {
              var e = t(l.resolveHref(i, a.href, !0), 2),
                o = e[0],
                n = e[1];
              return { href: o, as: a.as ? l.resolveHref(i, a.as) : n || o };
            },
            [i, a.href, a.as]
          ),
          h = b.href,
          p = b.as,
          c = a.children,
          f = a.replace,
          j = a.shallow,
          k = a.scroll,
          y = a.locale;
        'string' === typeof c && (c = n.default.createElement('a', null, c));
        var m =
            (e = n.default.Children.only(c)) && 'object' === typeof e && e.ref,
          w = t(r.useIntersection({ rootMargin: '200px' }), 2),
          v = w[0],
          x = w[1],
          g = n.default.useCallback(
            function (a) {
              v(a),
                m &&
                  ('function' === typeof m
                    ? m(a)
                    : 'object' === typeof m && (m.current = a));
            },
            [m, v]
          );
        n.default.useEffect(
          function () {
            var a = x && o && l.isLocalURL(h),
              e = 'undefined' !== typeof y ? y : i && i.locale,
              t = s[h + '%' + p + (e ? '%' + e : '')];
            a && !t && d(i, h, p, { locale: e });
          },
          [p, h, x, y, o, i]
        );
        var B = {
          ref: g,
          onClick: function (a) {
            e.props &&
              'function' === typeof e.props.onClick &&
              e.props.onClick(a),
              a.defaultPrevented ||
                (function (a, e, o, t, i, n, u, r) {
                  ('A' !== a.currentTarget.nodeName ||
                    (!(function (a) {
                      var e = a.currentTarget.target;
                      return (
                        (e && '_self' !== e) ||
                        a.metaKey ||
                        a.ctrlKey ||
                        a.shiftKey ||
                        a.altKey ||
                        (a.nativeEvent && 2 === a.nativeEvent.which)
                      );
                    })(a) &&
                      l.isLocalURL(o))) &&
                    (a.preventDefault(),
                    null == u && t.indexOf('#') >= 0 && (u = !1),
                    e[i ? 'replace' : 'push'](o, t, {
                      shallow: n,
                      locale: r,
                      scroll: u,
                    }));
                })(a, i, h, p, f, j, k, y);
          },
          onMouseEnter: function (a) {
            e.props &&
              'function' === typeof e.props.onMouseEnter &&
              e.props.onMouseEnter(a),
              l.isLocalURL(h) && d(i, h, p, { priority: !0 });
          },
        };
        if (a.passHref || ('a' === e.type && !('href' in e.props))) {
          var _ = 'undefined' !== typeof y ? y : i && i.locale,
            P =
              i &&
              i.isLocaleDomain &&
              l.getDomainLocale(p, _, i && i.locales, i && i.domainLocales);
          B.href = P || l.addBasePath(l.addLocale(p, _, i && i.defaultLocale));
        }
        return n.default.cloneElement(e, B);
      };
      e.default = b;
    },
    7190: function (a, e, o) {
      'use strict';
      function t(a, e) {
        return (
          (function (a) {
            if (Array.isArray(a)) return a;
          })(a) ||
          (function (a, e) {
            var o = [],
              t = !0,
              i = !1,
              n = void 0;
            try {
              for (
                var l, u = a[Symbol.iterator]();
                !(t = (l = u.next()).done) &&
                (o.push(l.value), !e || o.length !== e);
                t = !0
              );
            } catch (r) {
              (i = !0), (n = r);
            } finally {
              try {
                t || null == u.return || u.return();
              } finally {
                if (i) throw n;
              }
            }
            return o;
          })(a, e) ||
          (function () {
            throw new TypeError(
              'Invalid attempt to destructure non-iterable instance'
            );
          })()
        );
      }
      Object.defineProperty(e, '__esModule', { value: !0 }),
        (e.useIntersection = function (a) {
          var e = a.rootMargin,
            o = a.disabled || !l,
            r = i.useRef(),
            s = t(i.useState(!1), 2),
            d = s[0],
            b = s[1],
            h = i.useCallback(
              function (a) {
                r.current && (r.current(), (r.current = void 0)),
                  o ||
                    d ||
                    (a &&
                      a.tagName &&
                      (r.current = (function (a, e, o) {
                        var t = (function (a) {
                            var e = a.rootMargin || '',
                              o = u.get(e);
                            if (o) return o;
                            var t = new Map(),
                              i = new IntersectionObserver(function (a) {
                                a.forEach(function (a) {
                                  var e = t.get(a.target),
                                    o =
                                      a.isIntersecting ||
                                      a.intersectionRatio > 0;
                                  e && o && e(o);
                                });
                              }, a);
                            return (
                              u.set(
                                e,
                                (o = { id: e, observer: i, elements: t })
                              ),
                              o
                            );
                          })(o),
                          i = t.id,
                          n = t.observer,
                          l = t.elements;
                        return (
                          l.set(a, e),
                          n.observe(a),
                          function () {
                            l.delete(a),
                              n.unobserve(a),
                              0 === l.size && (n.disconnect(), u.delete(i));
                          }
                        );
                      })(
                        a,
                        function (a) {
                          return a && b(a);
                        },
                        { rootMargin: e }
                      )));
              },
              [o, e, d]
            );
          return (
            i.useEffect(
              function () {
                if (!l && !d) {
                  var a = n.requestIdleCallback(function () {
                    return b(!0);
                  });
                  return function () {
                    return n.cancelIdleCallback(a);
                  };
                }
              },
              [d]
            ),
            [h, d]
          );
        });
      var i = o(7294),
        n = o(9311),
        l = 'undefined' !== typeof IntersectionObserver;
      var u = new Map();
    },
    5608: function (a, e, o) {
      'use strict';
      o.d(e, {
        Z: function () {
          return l;
        },
      });
      var t = o(5893),
        i = o(9008),
        n = function (a) {
          var e = a.title;
          return (0, t.jsxs)(i.default, {
            children: [
              (0, t.jsx)('title', { children: e }),
              (0, t.jsx)('link', {
                href: 'https://fonts.googleapis.com/css2?family=Lato&display=swap',
                rel: 'stylesheet',
              }),
            ],
          });
        },
        l = function (a) {
          var e = a.children,
            o = a.title;
          return (0, t.jsxs)(t.Fragment, {
            children: [(0, t.jsx)(n, { title: o }), e],
          });
        };
    },
    9295: function (a, e, o) {
      'use strict';
      o.r(e),
        o.d(e, {
          default: function () {
            return D;
          },
        });
      var t = o(5893),
        i = o(7294);
      function n(a) {
        return (0, i.useMemo)(
          function () {
            return a
              .filter(function (a) {
                return a;
              })
              .join(' ');
          },
          [a]
        );
      }
      var l = (0, i.createContext)(0);
      function u() {
        return (0, i.useContext)(l);
      }
      var r = o(5880),
        s = o.n(r),
        d = function (a) {
          var e = a.children,
            o = a.className,
            i = u(),
            l = 'h'.concat(i);
          return (0, t.jsx)(l, { className: n([s().heading, o]), children: e });
        };
      var b = function (a) {
        var e = a.children,
          o = u() + 1;
        return (0, t.jsx)(l.Provider, { value: o, children: e });
      };
      function h(a) {
        return a.toLowerCase().replaceAll(' ', '-');
      }
      var p = o(9934),
        c = o.n(p),
        f = function (a) {
          var e = a.heading,
            o = a.Component,
            i = void 0 === o ? 'section' : o,
            l = a.className,
            u = a.children;
          return (0, t.jsx)(b, {
            children: (0, t.jsxs)(i, {
              id: h(e),
              className: n([c().section, l]),
              children: [(0, t.jsx)(d, { children: e }), u],
            }),
          });
        },
        j = o(3925),
        k = o.n(j),
        y = function (a) {
          var e = a.children,
            o = a.ordered,
            l = a.className,
            u = a.itemClassName,
            r = (0, i.useMemo)(
              function () {
                return o ? 'ol' : 'ul';
              },
              [o]
            );
          return (0, t.jsx)(r, {
            className: n([k().list, o && k().ordered, l]),
            children: i.Children.map(e, function (a, e) {
              return (0, t.jsx)('li', { className: u, children: a }, e);
            }),
          });
        },
        m = o(1664),
        w = o(518),
        v = o.n(w),
        x = function (a) {
          var e = a.children,
            o = a.target;
          return (0, t.jsx)(m.default, {
            href: o,
            children: (0, t.jsx)('a', { className: v().link, children: e }),
          });
        },
        g = function (a) {
          var e = a.heading;
          return (0, t.jsx)(x, { target: '#'.concat(h(e)), children: e });
        },
        B = o(61),
        _ = o.n(B),
        P = function (a) {
          var e = a.heading,
            o = a.children;
          return (0, t.jsxs)(t.Fragment, {
            children: [
              (0, t.jsx)(g, { heading: e }),
              (0, t.jsx)(M, { className: _().subsections, children: o }),
            ],
          });
        };
      function T(a) {
        return (function (a) {
          return i.Children.toArray(a).filter(C);
        })(a).map(L);
      }
      function C(a) {
        var e = a.props;
        return e && !!e.heading;
      }
      function L(a) {
        var e = a.props,
          o = e.heading,
          i = e.children;
        return (0, t.jsx)(P, { heading: o, children: i }, o);
      }
      var M = function (a) {
          var e = a.className,
            o = a.children;
          return (0, t.jsx)(y, { ordered: !0, className: e, children: T(o) });
        },
        N = function (a) {
          var e = a.children;
          return (0, t.jsx)(f, {
            heading: 'Table of Contents',
            children: (0, t.jsx)('nav', {
              children: (0, t.jsx)(M, { children: e }),
            }),
          });
        },
        E = function (a) {
          var e = a.heading,
            o = a.children;
          return (0, t.jsxs)(f, {
            Component: 'article',
            heading: e,
            children: [(0, t.jsx)(N, { children: o }), o],
          });
        },
        A = o(5608),
        D = function () {
          return (0, t.jsx)(A.Z, {
            title: 'Damian Zamolski',
            children: (0, t.jsxs)(E, {
              heading: 'Article',
              children: [
                (0, t.jsxs)(f, {
                  heading: 'Section A',
                  children: [
                    (0, t.jsx)('p', {
                      children:
                        'Minions ipsum bee do bee do bee do tatata bala tu ti aamoo! Jiji para t\xfa wiiiii hahaha jeje butt chasy. Jeje tatata bala tu po kass tank yuuu! Wiiiii hahaha. Para t\xfa poulet tikka masala potatoooo gelatooo tank yuuu! La bodaaa bee do bee do bee do gelatooo. Jiji belloo! Bananaaaa bee do bee do bee do poopayee.',
                    }),
                    (0, t.jsx)('p', {
                      children:
                        'Bee do bee do bee do poopayee uuuhhh poopayee. Aaaaaah tank yuuu! Tatata bala tu wiiiii poopayee la bodaaa. Tatata bala tu poulet tikka masala hana dul sae jeje ti aamoo! Belloo! pepete ti aamoo! Bananaaaa chasy bappleees daa uuuhhh underweaaar daa. Tatata bala tu bananaaaa gelatooo la bodaaa tulaliloo jeje. Tatata bala tu jeje hana dul sae aaaaaah poulet tikka masala hahaha baboiii para t\xfa jiji poulet tikka masala wiiiii.',
                    }),
                    (0, t.jsx)('p', {
                      children:
                        'Baboiii la bodaaa poopayee hana dul sae tatata bala tu bee do bee do bee do butt aaaaaah chasy me want bananaaa! Poulet tikka masala. Hana dul sae chasy bappleees gelatooo bappleees la bodaaa potatoooo daa hana dul sae tank yuuu! Belloo! Hahaha hahaha butt po kass butt. Ti aamoo! ti aamoo! Baboiii hana dul sae aaaaaah. Baboiii me want bananaaa! Me want bananaaa! Aaaaaah para t\xfa. Aaaaaah tank yuuu! Tank yuuu! Baboiii wiiiii poopayee belloo! Daa bee do bee do bee do. Bee do bee do bee do tank yuuu! Uuuhhh baboiii underweaaar wiiiii jiji daa tulaliloo tatata bala tu.',
                    }),
                    (0, t.jsxs)(f, {
                      heading: 'Section AA',
                      children: [
                        (0, t.jsx)('p', {
                          children:
                            'Minions ipsum potatoooo bananaaaa wiiiii ti aamoo! Pepete poopayee poopayee uuuhhh aaaaaah. Bee do bee do bee do tulaliloo bananaaaa tatata bala tu hana dul sae me want bananaaa! Ti aamoo! Tatata bala tu butt jiji bananaaaa. Daa potatoooo chasy poulet tikka masala underweaaar underweaaar poopayee. Poulet tikka masala butt gelatooo bee do bee do bee do. Poopayee chasy gelatooo la bodaaa bananaaaa belloo! Potatoooo po kass. Ti aamoo! tatata bala tu po kass po kass ti aamoo! Uuuhhh hahaha. Tulaliloo po kass belloo! Tatata bala tu uuuhhh butt tatata bala tu. Bananaaaa po kass daa baboiii po kass la bodaaa ti aamoo! Bappleees para t\xfa bananaaaa belloo! Jeje poopayee poulet tikka masala wiiiii jeje daa po kass tank yuuu! Chasy.',
                        }),
                        (0, t.jsx)('p', {
                          children:
                            'Bee do bee do bee do daa butt po kass underweaaar jeje aaaaaah. Butt baboiii la bodaaa jeje la bodaaa aaaaaah butt jeje chasy. Baboiii ti aamoo! Underweaaar jeje bappleees. Daa chasy pepete hana dul sae aaaaaah butt chasy po kass tatata bala tu baboiii. Hana dul sae tulaliloo para t\xfa poulet tikka masala. Tank yuuu! poopayee poulet tikka masala bappleees wiiiii bee do bee do bee do jiji tank yuuu! Pepete. Bappleees me want bananaaa! Butt bananaaaa pepete uuuhhh poulet tikka masala belloo! Jeje. Baboiii wiiiii gelatooo bananaaaa tulaliloo tank yuuu! La bodaaa tulaliloo.Minions ipsum potatoooo bananaaaa wiiiii ti aamoo! Pepete poopayee poopayee uuuhhh aaaaaah. Bee do bee do bee do tulaliloo bananaaaa tatata bala tu hana dul sae me want bananaaa! Ti aamoo! Tatata bala tu butt jiji bananaaaa. Daa potatoooo chasy poulet tikka masala underweaaar underweaaar poopayee. Poulet tikka masala butt gelatooo bee do bee do bee do. Poopayee chasy gelatooo la bodaaa bananaaaa belloo! Potatoooo po kass. Ti aamoo! tatata bala tu po kass po kass ti aamoo! Uuuhhh hahaha. Tulaliloo po kass belloo! Tatata bala tu uuuhhh butt tatata bala tu. Bananaaaa po kass daa baboiii po kass la bodaaa ti aamoo! Bappleees para t\xfa bananaaaa belloo! Jeje poopayee poulet tikka masala wiiiii jeje daa po kass tank yuuu! Chasy.',
                        }),
                        (0, t.jsx)('p', {
                          children:
                            'Gelatooo tulaliloo hahaha tank yuuu! Daa butt poulet tikka masala. Belloo! la bodaaa underweaaar me want bananaaa! Uuuhhh jeje para t\xfa chasy poopayee. Poopayee tulaliloo tank yuuu! Gelatooo bananaaaa butt bananaaaa. Butt me want bananaaa! Daa bee do bee do bee do baboiii potatoooo belloo! Jiji para t\xfa potatoooo potatoooo. Tulaliloo wiiiii bee do bee do bee do poopayee.',
                        }),
                      ],
                    }),
                  ],
                }),
                (0, t.jsxs)(f, {
                  heading: 'Section B',
                  children: [
                    (0, t.jsx)('p', {
                      children:
                        'Minions ipsum poopayee poulet tikka masala belloo! Uuuhhh tulaliloo bananaaaa. Poopayee la bodaaa daa jeje potatoooo para t\xfa pepete jiji gelatooo ti aamoo! Poulet tikka masala. Potatoooo bappleees bee do bee do bee do underweaaar jiji me want bananaaa! Potatoooo tulaliloo bee do bee do bee do tank yuuu! Poopayee poopayee pepete bananaaaa jeje chasy la bodaaa. Wiiiii hana dul sae hahaha tulaliloo.',
                    }),
                    (0, t.jsx)('p', {
                      children:
                        'Gelatooo tulaliloo uuuhhh uuuhhh aaaaaah jiji hahaha wiiiii wiiiii hahaha. Jiji belloo! Bappleees la bodaaa baboiii pepete. Potatoooo po kass hahaha ti aamoo! Hana dul sae tatata bala tu chasy. Gelatooo la bodaaa me want bananaaa! Butt hana dul sae poulet tikka masala tatata bala tu hahaha tank yuuu! Belloo! me want bananaaa! Potatoooo jiji bee do bee do bee do butt aaaaaah me want bananaaa! Uuuhhh. Bappleees gelatooo tulaliloo jiji chasy baboiii me want bananaaa! Belloo! Tulaliloo. Po kass underweaaar poulet tikka masala jiji me want bananaaa! Gelatooo la bodaaa jiji chasy bappleees chasy para t\xfa para t\xfa chasy daa poulet tikka masala.',
                    }),
                    (0, t.jsx)('p', {
                      children:
                        'Bee do bee do bee do butt bananaaaa tank yuuu! Potatoooo tatata bala tu hahaha po kass para t\xfa wiiiii. Tank yuuu! baboiii poopayee tulaliloo tatata bala tu jiji jeje bee do bee do bee do uuuhhh hana dul sae jeje. Hana dul sae tank yuuu! La bodaaa para t\xfa ti aamoo! Bee do bee do bee do wiiiii belloo! Bappleees. Po kass potatoooo para t\xfa tank yuuu! Tulaliloo baboiii wiiiii para t\xfa jiji.',
                    }),
                  ],
                }),
                (0, t.jsxs)(f, {
                  heading: 'Section C',
                  children: [
                    (0, t.jsx)('p', {
                      children:
                        'Minions ipsum bappleees poopayee tatata bala tu para t\xfa ti aamoo! Bee do bee do bee do bappleees. Tank yuuu! aaaaaah butt bee do bee do bee do baboiii tatata bala tu. Tatata bala tu butt belloo! Hana dul sae bananaaaa. Para t\xfa jeje tulaliloo bappleees po kass. La bodaaa la bodaaa bappleees potatoooo me want bananaaa! Jeje baboiii para t\xfa poulet tikka masala. La bodaaa aaaaaah daa tank yuuu! Wiiiii la bodaaa daa wiiiii wiiiii potatoooo wiiiii. Bee do bee do bee do tank yuuu! Chasy belloo!',
                    }),
                    (0, t.jsx)('p', {
                      children:
                        'Butt bananaaaa la bodaaa jeje uuuhhh bananaaaa poopayee. La bodaaa uuuhhh uuuhhh tatata bala tu underweaaar poulet tikka masala poopayee butt jiji gelatooo daa. Gelatooo potatoooo para t\xfa ti aamoo! Gelatooo aaaaaah butt baboiii poopayee la bodaaa tulaliloo. Hahaha po kass po kass aaaaaah wiiiii belloo! Hana dul sae hana dul sae tatata bala tu pepete. Chasy me want bananaaa! Hana dul sae pepete.',
                    }),
                    (0, t.jsx)('p', {
                      children:
                        'Jiji baboiii hahaha hana dul sae tatata bala tu jeje ti aamoo! Po kass pepete. Tatata bala tu pepete po kass wiiiii tank yuuu! Ti aamoo! Baboiii me want bananaaa! Baboiii poulet tikka masala tatata bala tu uuuhhh poopayee me want bananaaa! Daa me want bananaaa! Para t\xfa wiiiii. Butt uuuhhh gelatooo tank yuuu! Daa belloo! Aaaaaah hahaha la bodaaa daa. Wiiiii poulet tikka masala aaaaaah tatata bala tu poulet tikka masala underweaaar underweaaar. Underweaaar gelatooo baboiii bee do bee do bee do belloo! Jiji hana dul sae. Po kass belloo! Po kass daa aaaaaah tank yuuu! Daa.',
                    }),
                  ],
                }),
              ],
            }),
          });
        };
    },
    5880: function (a) {
      a.exports = { heading: 'Heading_heading__YUsIL' };
    },
    518: function (a) {
      a.exports = { link: 'Link_link__aNRlY' };
    },
    3925: function (a) {
      a.exports = { list: 'List_list__zDYmJ', ordered: 'List_ordered__vQCaL' };
    },
    9934: function (a) {
      a.exports = { section: 'Section_section__hbEMX' };
    },
    61: function (a) {
      a.exports = { subsections: 'TableOfContentsItem_subsections__d2QsW' };
    },
    9008: function (a, e, o) {
      a.exports = o(5443);
    },
    1664: function (a, e, o) {
      a.exports = o(8418);
    },
  },
  function (a) {
    a.O(0, [774, 888, 179], function () {
      return (e = 5301), a((a.s = e));
      var e;
    });
    var e = a.O();
    _N_E = e;
  },
]);
