(self.__BUILD_MANIFEST = {
  __rewrites: { beforeFiles: [], afterFiles: [], fallback: [] },
  '/': [
    'static/css/f043793c5b116f29.css',
    'static/chunks/pages/index-368348c74c2cda83.js',
  ],
  '/_error': ['static/chunks/pages/_error-2280fa386d040b66.js'],
  '/books/deep-work': [
    'static/chunks/pages/books/deep-work-bb00e82f74c7e598.js',
  ],
  '/tools/teams-generator': [
    'static/css/5ef6412d83bbcad6.css',
    'static/chunks/pages/tools/teams-generator-b893ad7d4d792d63.js',
  ],
  sortedPages: [
    '/',
    '/_app',
    '/_error',
    '/books/deep-work',
    '/tools/teams-generator',
  ],
}),
  self.__BUILD_MANIFEST_CB && self.__BUILD_MANIFEST_CB();
