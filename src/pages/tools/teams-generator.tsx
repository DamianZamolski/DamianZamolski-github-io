import { useCallback, useState } from 'react';
import Page from 'components/page';
import TextArea from 'components/text-area';
import generateTeams from '../../generateTeams';
import NumberInput from '@/NumberInput';

export default function () {
  const [playersText, setPlayersText] = useState<string>('');
  const [teamsCount, setTeamsCount] = useState<number>(2);
  const [teamsText, setTeamsText] = useState<string>('');

  const handleGenerateButtonClick = useCallback(() => {
    const playersInTiers = splitByLinesAndWords(playersText);
    const teams = generateTeams(playersInTiers, teamsCount);
    setPlayersText(joinLinesAndWords(playersInTiers));
    setTeamsText(joinLinesAndWords(teams));
  }, [playersText, setTeamsText, teamsCount]);

  return (
    <Page title='Teams Generator'>
      <TextArea value={playersText} setValue={setPlayersText} autoFocus />
      <NumberInput value={teamsCount} setValue={setTeamsCount} min={2} />
      <button onClick={handleGenerateButtonClick}>Generate</button>
      <TextArea value={teamsText} />
    </Page>
  );
}

function splitByLinesAndWords(string: string): string[][] {
  return string
    .split('\n')
    .filter((line) => line)
    .map((line) => line.trim().split(/\s+/));
}

function joinLinesAndWords(lines: string[][]): string {
  return lines.map((line) => line.join(' ')).join('\n');
}
