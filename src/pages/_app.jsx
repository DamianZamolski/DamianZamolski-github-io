import '../app.css';

const App = ({ Component, pageProps }) => <Component {...pageProps} />;

export default App;
