export default function shuffle(objects: any[]) {
  const shuffledObjects = [...objects];
  for (let i = shuffledObjects.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffledObjects[i], shuffledObjects[j]] = [
      shuffledObjects[j],
      shuffledObjects[i],
    ];
  }
  return shuffledObjects;
}
