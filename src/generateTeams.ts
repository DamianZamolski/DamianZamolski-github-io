import shuffle from './shuffle';

export default function generateTeams(
  playersInTiers: string[][],
  teamsCount: number
): string[][] {
  const players: string[] = playersInTiers.map(shuffle).flat();
  const teams: string[][] = Array.from({ length: teamsCount }, () => []);
  while (players.length) {
    teams.forEach((team) => {
      const player = players.shift();
      if (player) team.push(player);
    });
    players.reverse();
  }
  return teams.filter((team) => team.length);
}
