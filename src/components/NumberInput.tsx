import { useCallback } from 'react';

interface NumberInputProps {
  value: number;
  setValue: Function;
  min?: number;
  max?: number;
}

export default function NumberInput({
  value,
  setValue,
  min,
  max,
}: NumberInputProps) {
  const handleInput = useCallback(
    ({
      target: {
        validity: { valid },
        valueAsNumber,
      },
    }) => {
      if (valid) setValue(valueAsNumber);
    },
    []
  );

  return (
    <input
      type='number'
      value={value}
      onInput={handleInput}
      min={min}
      max={max}
    />
  );
}
