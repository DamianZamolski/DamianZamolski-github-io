import { createContext } from 'react';

const LevelContext = createContext<number>(0);

export default LevelContext;
