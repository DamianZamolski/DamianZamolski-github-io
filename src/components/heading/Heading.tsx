import { FC } from 'react';
import Component from '@/Component';
import useClassName from '@/useClassName';
import useLevel from '@/useLevel';
import classes from './Heading.module.css';

interface HeadingProps {
  className?: string;
}

const Heading: FC<HeadingProps> = ({ children, className }) => {
  const level = useLevel() as 1 | 2 | 3 | 4 | 5 | 6;
  const Component: Component = `h${level}`;

  return (
    <Component className={useClassName([classes.heading, className])}>
      {children}
    </Component>
  );
};

export default Heading;
