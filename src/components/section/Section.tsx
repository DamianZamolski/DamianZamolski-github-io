import { FC } from 'react';
import Component from '@/Component';
import Heading from '@/heading';
import NextLevelProvider from '@/NextLevelProvider';
import useClassName from '@/useClassName';
import getIdFromHeading from '@/getIdFromHeading';
import classes from './Section.module.css';

interface SectionProps {
  heading: string;
  Component?: Component;
  className?: string;
}

const Section: FC<SectionProps> = ({
  heading,
  Component = 'section',
  className,
  children,
}) => (
  <NextLevelProvider>
    <Component
      id={getIdFromHeading(heading)}
      className={useClassName([classes.section, className])}
    >
      <Heading>{heading}</Heading>
      {children}
    </Component>
  </NextLevelProvider>
);

export default Section;
