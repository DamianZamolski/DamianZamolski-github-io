import { FC } from 'react';

interface CodeProps {
  children: string;
}

const Code: FC<CodeProps> = ({ children }) => (
  <pre>
    <code>{children}</code>
  </pre>
);

export default Code;
