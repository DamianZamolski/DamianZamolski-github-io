import { FC } from 'react';
import PageHead from './PageHead';

interface PageProps {
  title: string;
}

const Page: FC<PageProps> = ({ children, title }) => (
  <>
    <PageHead title={title} />
    {children}
  </>
);

export default Page;
