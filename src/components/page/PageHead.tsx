import { FC } from 'react';
import Head from 'next/head';

interface PageHeadProps {
  children?: never;
  title: string;
}

const PageHead: FC<PageHeadProps> = ({ title }) => (
  <Head>
    <title>{title}</title>
    <link
      href='https://fonts.googleapis.com/css2?family=Lato&display=swap'
      rel='stylesheet'
    />
  </Head>
);

export default PageHead;
