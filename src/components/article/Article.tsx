import { FC } from 'react';
import Section from '@/section';
import TableOfContents from '@/table-of-contents';

interface ArticleProps {
  heading: string;
}

const Article: FC<ArticleProps> = ({ heading, children }) => (
  <Section Component='article' heading={heading}>
    <TableOfContents>{children}</TableOfContents>
    {children}
  </Section>
);

export default Article;
