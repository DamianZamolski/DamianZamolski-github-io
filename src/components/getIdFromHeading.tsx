export default function getIdFromHeading(heading: string): string {
  return heading.toLowerCase().replaceAll(' ', '-');
}
