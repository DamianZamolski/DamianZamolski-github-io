import { Children, FC, useMemo } from 'react';
import Component from '@/Component';
import useClassName from '@/useClassName';
import classes from './List.module.css';

interface ListProps {
  ordered?: boolean;
  className?: string;
  itemClassName?: string;
}

const List: FC<ListProps> = ({
  children,
  ordered,
  className,
  itemClassName,
}) => {
  const Component: Component = useMemo(
    () => (ordered ? 'ol' : 'ul'),
    [ordered]
  );
  return (
    <Component
      className={useClassName([
        classes.list,
        ordered && classes.ordered,
        className,
      ])}
    >
      {Children.map(children, (child, index) => (
        <li className={itemClassName} key={index}>
          {child}
        </li>
      ))}
    </Component>
  );
};
export default List;
