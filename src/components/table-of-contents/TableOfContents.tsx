import { FC } from 'react';
import Section from '@/section';
import TableOfContentsItems from './TableOfContentsItems';

const TableOfContents: FC = ({ children }) => (
  <Section heading='Table of Contents'>
    <nav>
      <TableOfContentsItems>{children}</TableOfContentsItems>
    </nav>
  </Section>
);

export default TableOfContents;
