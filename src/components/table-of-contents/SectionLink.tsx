import { FC } from 'react';
import Link from '@/link';
import getIdFromHeading from '@/getIdFromHeading';

interface SectionLinkProps {
  heading: string;
  children?: never;
}

const SectionLink: FC<SectionLinkProps> = ({ heading }) => (
  <Link target={`#${getIdFromHeading(heading)}`}>{heading}</Link>
);

export default SectionLink;
