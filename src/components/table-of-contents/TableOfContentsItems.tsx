import { Children, FC, ReactNode } from 'react';
import List from '@/list';
import TableOfContentsItem from './TableOfContentsItem';

function findItems(children: ReactNode): ReactNode {
  return findSections(children).map(mapToItem);
}

function findSections(children: ReactNode): Section[] {
  return Children.toArray(children).filter(isSection);
}

interface Section {
  props: { heading: string; children?: ReactNode };
}

function isSection(child: ReactNode): child is Section {
  const { props } = child as Section;
  return props && !!props.heading;
}

function mapToItem({ props: { heading, children } }: Section): ReactNode {
  return (
    <TableOfContentsItem heading={heading} key={heading}>
      {children}
    </TableOfContentsItem>
  );
}

interface TableOfContentsItemsProps {
  className?: string;
}

const TableOfContentsItems: FC<TableOfContentsItemsProps> = ({
  className,
  children,
}) => (
  <List ordered className={className}>
    {findItems(children)}
  </List>
);

export default TableOfContentsItems;
