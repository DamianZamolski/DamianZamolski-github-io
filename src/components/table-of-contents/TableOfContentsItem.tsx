import { FC } from 'react';
import SectionLink from './SectionLink';
import TableOfContentsItems from './TableOfContentsItems';
import classes from './TableOfContentsItem.module.css';

interface TableOfContentsItemProps {
  heading: string;
}

const TableOfContentsItem: FC<TableOfContentsItemProps> = ({
  heading,
  children,
}) => (
  <>
    <SectionLink heading={heading} />
    <TableOfContentsItems className={classes.subsections}>
      {children}
    </TableOfContentsItems>
  </>
);

export default TableOfContentsItem;
