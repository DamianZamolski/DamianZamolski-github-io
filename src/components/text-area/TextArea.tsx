import { useCallback, useEffect, useMemo, useRef } from 'react';
import classes from './TextArea.module.css';

interface TextAreaProps {
  value: string;

  autoFocus?: boolean;
  setValue?: Function;
}

export default function TextArea({
  autoFocus,
  setValue,
  value,
}: TextAreaProps) {
  const ref = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    if (autoFocus && ref.current) ref.current.focus();
  }, []);

  const handleChange = useCallback(
    (event) => {
      if (setValue) setValue(event.target.value);
    },
    [setValue]
  );

  const rows: number = useMemo(() => value.split('\n').length, [value]);

  return (
    <textarea
      ref={ref}
      className={classes['text-area']}
      onChange={handleChange}
      readOnly={!setValue}
      rows={rows}
      value={value}
    />
  );
}
