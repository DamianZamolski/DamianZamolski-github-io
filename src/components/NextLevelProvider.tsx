import { FC } from 'react';
import LevelContext from '@/LevelContext';
import useNextLevel from '@/useNextLevel';

const NextLevelProvider: FC = ({ children }) => {
  const nextLevel: number = useNextLevel();
  return (
    <LevelContext.Provider value={nextLevel}>{children}</LevelContext.Provider>
  );
};

export default NextLevelProvider;
