import { useContext } from 'react';
import LevelContext from '@/LevelContext';

export default function useLevel(): number {
  return useContext(LevelContext);
}
