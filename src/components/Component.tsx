type Component = keyof JSX.IntrinsicElements;

export default Component;
