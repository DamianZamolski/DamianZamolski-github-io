import useLevel from '@/useLevel';

export default function useNextLevel(): number {
  return useLevel() + 1;
}
