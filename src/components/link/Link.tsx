import { FC } from 'react';
import NextLink from 'next/link';
import classes from './Link.module.css';

interface LinkProps {
  target: string;
}

const Link: FC<LinkProps> = ({ children, target }) => (
  <NextLink href={target}>
    <a className={classes.link}>{children}</a>
  </NextLink>
);

export default Link;
