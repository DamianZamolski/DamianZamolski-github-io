import { useMemo } from 'react';

export default function useClassName(
  classNames: (string | boolean | undefined)[]
): string {
  return useMemo(
    () => classNames.filter((className) => className).join(' '),
    [classNames]
  );
}
